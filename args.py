from argparse import ArgumentParser
import transforms as tx

from os import path


def make_out_path(in_path):
    base, ext = path.splitext(in_path)
    return base + "_out" + ext


def action_tuple(first, type):
    def pair2(second):
        return first, type(second)

    return pair2


parser = ArgumentParser(description="Semestrálka na BI-PYT: grafické operace pomocí NumPy.",
                        epilog="Adam Platkevič (platkada@fit.cvut.cz) 2018")

transforms_group = parser.add_argument_group("dílčí transformace",
                                             """
                                             Tyto argumenty lze uvést v libovolném počtu a pořadí. Odpovídající operace 
                                             budou v tomto pořadí také provedeny.
                                             """)

parser.add_argument("file")
parser.add_argument("-o", "--out",
                    dest="out_file",
                    metavar="path",
                    help="cesta výstupního obrázku; výchozí hodnota je původní cesta doplněná o \"_out\"")

transforms_group.add_argument("-i", "--invert",
                              action="append_const",
                              dest="actions",
                              const=(tx.invert,),
                              help="inverze barev")
transforms_group.add_argument("-g", "--grayscale",
                              action="append_const",
                              dest="actions",
                              const=(tx.grayscale,),
                              help="stupně šedi")
transforms_group.add_argument("-r", "--rotate",
                              action="append",
                              dest="actions",
                              nargs="?",
                              const=(tx.rotate, 1),
                              type=action_tuple(tx.rotate, int),
                              metavar="k",
                              help="rotace o k-násobek 90°; výchozí hodnota je 1 (90° CCW)")
transforms_group.add_argument("-H", "--fliph",
                              action="append_const",
                              dest="actions",
                              const=(tx.flip_h,),
                              help="horizontální překlopení")
transforms_group.add_argument("-V", "--flipv",
                              action="append_const",
                              dest="actions",
                              const=(tx.flip_v,),
                              help="vertikální překlopení")
transforms_group.add_argument("-b", "--brightness",
                              action="append",
                              dest="actions",
                              nargs="?",
                              const=(tx.brightness, 2),
                              type=action_tuple(tx.brightness, float),
                              metavar="coef",
                              help="násobení světlosti; výchozí hodnota je 2")
transforms_group.add_argument("-s", "--sharpen",
                              action="append",
                              dest="actions",
                              nargs="?",
                              const=(tx.sharpen, 1),
                              type=action_tuple(tx.sharpen, float),
                              metavar="amt",
                              help="zaostření; výchozí hodnota je 1")
transforms_group.add_argument("-G", "--gamma",
                              action="append",
                              dest="actions",
                              type=action_tuple(tx.gamma, float),
                              metavar="gamma",
                              help="gama korekce")
transforms_group.add_argument("-p", "--posterize",
                              action="append",
                              dest="actions",
                              nargs="?",
                              const=(tx.posterize, 1),
                              type=action_tuple(tx.posterize, int),
                              metavar="levels",
                              help="posterizace; výchozí hodnota je 1")
transforms_group.add_argument("-S", "--saturation",
                              action="append",
                              dest="actions",
                              type=action_tuple(tx.saturation, float),
                              metavar="amt",
                              help="násobení sytosti")
transforms_group.add_argument("-v", "--vintage",
                              action="append_const",
                              dest="actions",
                              const=(tx.sepia,),
                              help="sépie")


def get_args():
    args = vars(parser.parse_args())
    if not args["out_file"]:
        args["out_file"] = make_out_path(args["file"])
    return args
