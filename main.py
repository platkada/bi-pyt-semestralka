import time
from PIL import Image
import numpy as np

from args import get_args

cmd_args = get_args()

# načtení obrázku
img = Image.open(cmd_args["file"])
data = np.asarray(img, dtype=np.float) / 255

start = time.time()

if cmd_args["actions"]:
    for action, *args in cmd_args["actions"]:
        data = action(data, *args)

end = time.time()
print("duration:", end - start)

# uložení výstupu
out_img = np.clip(data * 255, 0, 255)
out_img = np.asarray(out_img, dtype=np.uint8)
img_out = Image.fromarray(out_img, 'RGB')
img_out.save(cmd_args["out_file"])
