import numpy as np

grayscale_matrix = np.array([0.2126, 0.7152, 0.0722])

sepia_matrix = np.array([
    [.393, .769, .189],
    [.349, .686, .168],
    [.272, .534, .131]
])


def from_srgb(data):
    mask = data <= 0.04045
    data[mask] /= 12.92
    mask = ~mask
    data[mask] += 0.055
    data[mask] /= 1.055
    data[mask] **= 2.4


def to_srgb(data):
    mask = data <= 0.0031308
    data[mask] *= 12.92
    mask = ~mask
    data[mask] **= (1 / 2.4)
    data[mask] *= 1.055
    data[mask] -= 0.055


def invert(data):
    return 1 - data


def grayscale(data):
    from_srgb(data)
    # for rgb in data.reshape(-1, 3):
    #     rgb[...] = grayscale_matrix @ rgb.T
    data = np.einsum("ijk,k->ij", data, grayscale_matrix).repeat(3, 1).reshape(data.shape)
    to_srgb(data)
    return data


def sepia(data):
    from_srgb(data)
    data = np.einsum("ijl,kl->ijk", data, sepia_matrix)
    to_srgb(data)
    return data


def rotate(data, k):
    return np.rot90(data, k, (0, 1))


def flip_h(data):
    return np.fliplr(data)


def flip_v(data):
    return np.flipud(data)


def brightness(data, amt):
    data *= amt
    return data


def sharpen(data, amt):
    data[1:-1, 1:-1, :] += amt * (
            -data[0:-2, 0:-2] - data[0:-2, 1:-1] - data[0:-2, 2:] -
            data[1:-1, 0:-2] + 8 * data[1:-1, 1:-1] - data[1:-1, 2:] -
            data[2:, 0:-2] - data[2:, 1:-1] - data[2:, 2:]
    )
    return data


def gamma(data, g=1):
    from_srgb(data)
    data **= g
    to_srgb(data)
    return data


def posterize(data, levels):
    data = np.round(data * levels) / levels
    return data


def saturation(data, amt):
    lum = ((data.max(2) + data.min(2)) / 2).reshape(data.shape[:2]+(1,))
    data = (data - lum) * amt + lum
    return data
